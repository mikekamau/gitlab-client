package ru.terrakok.gitlabclient.entity.app.target

/**
 * Created by Konstantin Tskhovrebov (aka @terrakok) on 03.01.18.
 */
enum class TargetBadgeIcon {
    COMMENTS,
    UP_VOTES,
    DOWN_VOTES,
    COMMITS;
}